## Теоретический вопрос
Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript

В JS Каждый объект имеет внутреннюю ссылку на другой объект, который находится выше в иерархии, и называется его прототипом. У объекта-прототипа также имеется свой собственный прототип и цепь наследования идет до тех пор не завершится объектом, у которого свойство prototype равно null.  По определению, null не имеет прототипа и является завершающим звеном в цепочке прототипов. Соответственно, в JS любая функция может быть добавлена к объекту в виде его свойства. В итоге, вызывая какой-либо метод на объекте внизу иерархии, будет происходить поиск метода сначала в заданном методе и в случае отсутствия такового, поиск будет осуществляться ступенькой выше до тех пор, пока не дойдет до объекта со значением prototype null.


## Задание
1. Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта. 
2. Создайте геттеры и сеттеры для этих свойств.
3. Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
4. Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.
5. Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.

## Примечание
Задание должно быть выполнено на "чистом" Javascript без использования библиотек типа jQuery или React.

