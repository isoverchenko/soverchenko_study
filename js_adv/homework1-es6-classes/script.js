"use strict";

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  set name(name) {
    this._name = name;
  }
  get name() {
    return this._name;
  }
  set age(age) {
    this._age = age;
  }
  get age() {
    return this._age;
  }
  set salary(salary) {
    this._salary = salary;
  }
  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return this._salary;
  }
  set salary(salary) {
    this._salary = salary * 3;
  }
}

const Ivan = new Programmer("Ivan", 30, 3900, ["php", "python"]);
const Andrew = new Programmer("Andrew", 31, 4300, ["js", "go", "php"]);
const Dmitry = new Programmer("Dmitry", 30, 400, ["js"]);

console.log(Ivan, Andrew, Dmitry);
