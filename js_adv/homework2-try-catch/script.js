"use strict";

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

function checkTheBook({ author, name, price }, index) {
  if (!author) {
    throw new Error(`Author is not found (book number ${index + 1})`);
  }
  if (!name) {
    throw new Error(`Name of the book is not found (book number ${index + 1})`);
  }
  if (!price) {
    throw new Error(`Price is not found (book number ${index + 1})`);
  }
}

document.getElementById("root").innerHTML = "<ul id='ul'></ul>";

books.forEach((element, index) => {
  try {
    checkTheBook(element, index);
    document.getElementById(
      "ul"
    ).innerHTML += `<li> <ul> <li> Author: ${element.author} </li> <li> Name: ${element.name} </li> <li> Price: ${element.price}</li> </ul> </li>`;
  } catch (error) {
    console.log(error.message);
  }
});
