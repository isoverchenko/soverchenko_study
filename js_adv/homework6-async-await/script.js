const button = document.querySelector("button");
const text = document.querySelector(".location");

async function getRequest() {
  const result = await fetch("https://api.ipify.org/?format=json");
  const data = await result.json();
  console.log(data);
  return data;
}

async function getLocation(ip) {
  const result = await fetch(
    `http://ip-api.com/json/${ip}?fields=continent,country,region,city,district;lang=ru`
  );
  const data = await result.json();
  console.log(data);
  return data;
}

function renderLocation(data) {
  const textContent = `Тебя таки вычислили по айпи: ${data.continent}, ${data.country}, ${data.region}, город ${data.city}`;
  text.innerText = textContent;
  return text;
}

button.addEventListener("click", function (event) {
  event.preventDefault();
  getRequest()
    .then((resolve) => getLocation(resolve.ip))
    .then((resolve) => renderLocation(resolve));
});
