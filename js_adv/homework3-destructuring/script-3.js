"use strict";

//task 3

const user = {
  name: "John",
  years: 30,
  // isAdmin: true,
};

const { name, years, isAdmin = false } = user;
console.log(name, years, isAdmin);

//task 4

const satoshi2020 = {
  name: "Nick",
  surname: "Sabo",
  age: 51,
  country: "Japan",
  birth: "1979-08-21",
  location: {
    lat: 38.869422,
    lng: 139.876632,
  },
};

const satoshi2019 = {
  name: "Dorian",
  surname: "Nakamoto",
  age: 44,
  hidden: true,
  country: "USA",
  wallet: "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa",
  browser: "Chrome",
};

const satoshi2018 = {
  name: "Satoshi",
  surname: "Nakamoto",
  technology: "Bitcoin",
  country: "Japan",
  browser: "Tor",
  birth: "1975-04-05",
};

const fullProfile = {
  ...satoshi2018,
  ...satoshi2019,
  ...satoshi2020,
};

console.log(fullProfile);

//task 5

const books = [
  {
    name: "Harry Potter",
    author: "J.K. Rowling",
  },
  {
    name: "Lord of the rings",
    author: "J.R.R. Tolkien",
  },
  {
    name: "The witcher",
    author: "Andrzej Sapkowski",
  },
];

const bookToAdd = {
  name: "Game of thrones",
  author: "George R. R. Martin",
};

const booksNew = [...books, bookToAdd];
console.log(booksNew);

//task 6

const employee = {
  name: "Vitalii",
  surname: "Klichko",
};

const newEmployee = {
  ...employee,
  age: 44,
  salary: 2600,
};

console.log(newEmployee);

//task 7

const array = ["value", () => "showValue"];

// const value = array[0];
// const showValue = array[1];
// OR

const [value, showValue] = array;

alert(value); // должно быть выведено 'value'
alert(showValue()); // должно быть выведено 'showValue'
