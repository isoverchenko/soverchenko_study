"use strict";

const root = document.getElementById("root");

class Card {
  constructor(id, title, description, fullName, email) {
    this.id = id + 1;
    this.title = title;
    this.description = description;
    this.fullName = fullName;
    this.email = email;
  }
  postRender() {
    const post = `<div class="post" id="${this.id}">
    <button id="delete${this.id}" onclick="deletePost()">X</button> 
    <div class="post__wrapper">
        <h3 class="post__author author"> author: ${this.fullName}</h3>
        <h3 class="author__email"> e-mail: ${this.email.toLowerCase()}</h3>
        <p class="post__title">${this.title}</p>
        <p class="post__description">${this.description}</p>
        </div>`;
    root.innerHTML += post;
  }
}

async function loadContent() {
  const posts = await axios.get("https://ajax.test-danit.com/api/json/posts");
  const users = await axios.get("https://ajax.test-danit.com/api/json/users");
  for (let i = 0; i < posts.data.length; i++) {
    const postEl = posts.data[i];
    const indexRandom = Math.floor(Math.random() * users.data.length);
    const post = new Card(
      i,
      postEl.title,
      postEl.body,
      users.data[indexRandom].name,
      users.data[indexRandom].email
    );
    post.postRender();
  }
}
loadContent();

async function deletePost() {
  const button = event.target;
  const id = button.getAttribute("id").match(/(\d+)/)[0];
  const post = document.getElementById(id);
  const del = await axios.delete(
    `https://ajax.test-danit.com/api/json/posts/${id}`
  );
  if (del.status === 200) {
    setTimeout(() => post.remove(), 600);
  }
}
