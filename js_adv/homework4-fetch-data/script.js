"use strict";

const request = fetch("https://ajax.test-danit.com/api/swapi/films");

request
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    const root = document.getElementById("root");
    const filmsList = document.createElement("ul");
    data.map((element) => {
      console.log(element);
      const filmEl = document.createElement("li");
      const title = document.createElement("h2");
      const description = document.createElement("p");
      const charactersList = document.createElement("ul");
      const { id, name, openingCrawl } = element;
      console.log(openingCrawl);
      title.textContent = `Episode ${id} ${name}`;
      description.textContent = openingCrawl;
      filmEl.append(title);
      filmEl.append(description);
      filmEl.append(charactersList);
      console.log(filmEl);
      filmsList.append(filmEl);
      root.append(filmsList);

      element.characters.forEach((character) => {
        console.log(character);
        const characterEl = document.createElement("li");
        const characterResolve = fetch(character);
        characterResolve
          .then((response) => {
            return response.json();
          })
          .then((data) => {
            characterEl.textContent = data.name;
            charactersList.append(characterEl);
          });
      });
    });
  });
